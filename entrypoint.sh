#!/usr/bin/env bash

Xvfb :0 -ac -screen 0 1024x768x24 &

export DISPLAY=:99

exec "$@"
